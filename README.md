# ThemWorks README

##HTML

###MAIN SECTIONS
 * Three vertical sections are `div#header`, `div#content`,  and `div#footer`. Inside of each section, there's a `div.inner` element, this is for styling. 
 
**Note:** every closing tag has an associated comment, which describes the element that is being closed. It’s best practice to always comment out important parts or sections in an HTML document. We’ll follow this practice today.
		 
###HEADER SECTION
* ul.top
	* the last child of `ul.top` holds the search form 
	* two immediate children: 
		* `div#logo`
		* `ul#menu` **Note:**Has two `li` items with classes to mark active and dropdown items respectivly.
			* `li.active`
			* `li.dropdown` 

###CONTENT SECTION
* `#slider` 	
	* Three images
	* `div#cyclePager` for slider navigation 
* `.headline`
	* uses class for large headline text
* `promo-colo-3` 
	* the third `li` is `.testimonial`

###Footer
* `.top` - `.social`
* `.bottom`